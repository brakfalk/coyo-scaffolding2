(function () {
  'use strict';

  var login = require('../../login.page.js');
  var userDirectoryList = require('../admin/user-directories/user-directories-list.page.js');
  var userDirectoryDetails = require('../admin/user-directories/user-directories-details.page.js');
  var authProviderList = require('../admin/auth-provider/auth-provider-list.page.js');
  var authProviderDetails = require('../admin/auth-provider/auth-provider-details.page.js');
  var samlProviderDetails = require('../admin/auth-provider/saml-details.page.js');
  var components = require('../../components.page.js');
  var samlLoginPage = require('./saml-login.page.js');
  var Navigation = require('../navigation.page.js');

  /*
  var fs = require('fs');
  var path = require('path');

  function sendLongText(el, text) {
    var chunkSize = 200;
    for (var i = 0; i < text.length; i += chunkSize) {
      el.sendKeys(text.substr(i, chunkSize));
    }
  }
  */

  // disabled until COYOFOUR-6813 will be merged
  // eslint-disable-next-line jasmine/no-disabled-tests
  xdescribe('saml login', function () {
    var navigation, key/*, privateKey, certificate*/;

    beforeAll(function () {
      navigation = new Navigation();
      key = Math.ceil(Math.random() * 1000000);
      /*
      privateKey = fs.readFileSync(path.join(__dirname, 'private-key.pem'), 'utf-8');
      certificate = fs.readFileSync(path.join(__dirname, 'certificate.pem'), 'utf-8');
      */

      login.loginDefaultUser();

      // create ldap user directory
      navigation.profileMenu.open();
      navigation.profileMenu.admin.click();
      navigation.admin.userDirectories.click();
      userDirectoryList.createButton.click();
      var ldapName = 'LDAP_' + key;
      userDirectoryDetails.name.sendKeys(ldapName);
      $('.ui-select-container').click(); // TODO
      // userDirectoryDetails.type.openDropdown();
      // userDirectoryDetails.type.search('LDAP');
      $('.ui-select-container input[type="search"]').sendKeys('LDAP');
      userDirectoryDetails.type.selectOption('LDAP');
      // userDirectoryDetails.active.click();
      userDirectoryDetails.saveButton.click();

      // create saml auth provider
      navigation.admin.authProviders.click();
      // delete Test Saml if it already exists, because any previous saml test failed
      deleteSamlAuthProvider();
      authProviderList.createButton.click();
      authProviderDetails.name.sendKeys('Test Saml'); // must be fix
      element(by.model('$ctrl.authenticationProvider.type')).click(); // TODO
      $('.ui-select-container input[type="search"]').sendKeys('SAML');
      authProviderDetails.type.selectOption('SAML');
      authProviderDetails.active.click();
      samlProviderDetails.generalTab.entityId.sendKeys(samlLoginPage.idpBaseUrl.replace('https://', 'http://') + '/simplesaml/saml2/idp/metadata.php');
      samlProviderDetails.generalTab.authenticationEndpoint.sendKeys(samlLoginPage.idpBaseUrl + '/simplesaml/saml2/idp/SSOService.php');
      samlProviderDetails.generalTab.logoutEndpoint.sendKeys(samlLoginPage.idpBaseUrl + '/simplesaml/saml2/idp/SingleLogoutService.php');
      element(by.model('$ctrl.ngModel.properties.logoutMethod')).click(); // TODO
      samlProviderDetails.generalTab.logoutMethod.selectOption('Local');
      samlProviderDetails.generalTab.authenticationExpiryInSeconds.sendKeys('300');
      element(by.model('$ctrl.ngModel.properties.userDirectory')).click(); // TODO
      // $$('.ui-select-container input[type="search"]').get(1).sendKeys('LDAP');
      // samlProviderDetails.userDirectory.selectOption('LDAP');
      $$('.ui-select-container input[type="search"]').get(2).sendKeys(ldapName);
      samlProviderDetails.generalTab.userDirectory.selectOption(ldapName);

      // sign requests, is ignored by local idp
      // samlProviderDetails.signRequestsTab.heading.click();
      // samlProviderDetails.signRequestsTab.signRequests.click();
      // sendLongText(samlProviderDetails.signRequestsTab.signingCertificate, certificate);
      // sendLongText(samlProviderDetails.signRequestsTab.signingPrivateKey, privateKey);

      samlProviderDetails.validateResponseTab.heading.click();
      samlProviderDetails.validateResponseTab.disableTrustCheck.click();
      samlProviderDetails.generalTab.heading.click();
      authProviderDetails.saveButton.click();

      login.logout();
    });

    // eslint-disable-next-line jasmine/no-disabled-tests
    xit('login with saml', function () {
      // go to login page
      login.get();

      // click login with saml
      samlLoginPage.trigger('Test Saml');

      // login in saml idp
      samlLoginPage.login('nf', 'gocoyo');

      // verify logged in user
      expect($('.welcome-widget strong').getText()).toBe('Nancy Fork');
    });

    // eslint-disable-next-line jasmine/no-disabled-tests
    xit('show error message for unknown user', function () {
      // go to login page
      login.get();

      // click login with saml
      samlLoginPage.trigger('Test Saml');

      // login in saml idp
      samlLoginPage.login('xyz', 'gocoyo');

      // verify user was not logged in
      var deText = 'Authentifizierung fehlgeschlagen';
      var enText = 'Authentication Failed';
      expect(login.errorMessage.isDisplayed()).toBeTruthy();
      login.errorMessage.getText().then(function (text) {
        var isFailedAuthententication = (text === deText || text === enText);
        expect(isFailedAuthententication).toBeTruthy();
      });
    });

    afterEach(function () {
      samlLoginPage.idpLogout();
    });

    afterAll(function () {
      login.loginDefaultUser();

      // delete saml auth provider
      deleteSamlAuthProvider();

      // delete ldap user directory
      userDirectoryList.get();
      userDirectoryList.nameFilter.sendKeys('LDAP_' + key);
      userDirectoryList.table.rows.count().then(function (count) {
        if (count > 0) {
          userDirectoryList.table.rows.get(0).options().open();
          userDirectoryList.table.rows.get(0).options().deleteOption.click();
          components.modals.confirm.confirmButton.click();
        }
      });
    });

    function deleteSamlAuthProvider() {
      authProviderList.get();
      authProviderList.nameFilter.sendKeys('Test Saml');
      authProviderList.table.rows.count().then(function (count) {
        if (count > 0) {
          authProviderList.table.rows.get(0).options().open();
          authProviderList.table.rows.get(0).options().deleteOption.click();
          components.modals.confirm.confirmButton.click();
        }
      });
    }
  });

})();
