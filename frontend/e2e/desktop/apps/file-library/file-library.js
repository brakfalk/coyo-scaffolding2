(function () {
  'use strict';

  var PageDetails = require('./../../pages/page-details.page');
  var App = require('./../app.page');
  var AppSettings = require('./../app-settings.page');

  var login = require('./../../../login.page.js');
  var testhelper = require('./../../../testhelper.js');
  var component = require('./../../../components.page');

  describe('file library app', function () {
    var pageDetails, app, appSettings;

    beforeAll(function () {
      pageDetails = new PageDetails();
      app = new App();
      appSettings = new AppSettings();
      login.loginDefaultUser();
    });

    afterEach(function () {
      testhelper.deletePages();
    });

    it('should show error message when slug does not match pattern', function () {
      var key = Math.floor(Math.random() * 1000000);
      // create a page and file library app
      testhelper.createAndOpenPage('FileLibraryTestPage' + key);
      pageDetails.options.addApp.click();
      app.app.fileLibrary.click();
      appSettings.name.clear();
      appSettings.name.sendKeys('New_library_app');
      app.app.save.click();

      // change app settings
      var el = app.appNav.groupPanelElements(0).first();
      app.appNav.hover(el);
      app.appNav.openAppSettings(el);

      expect(appSettings.slug.getAttribute('value')).toBe('new-library-app');

      appSettings.slug.clear();
      expect(appSettings.slug.getAttribute('value')).toBe('');

      appSettings.slug.sendKeys('new_library_app');
      expect(appSettings.slugErrorMessage.isPresent()).toBeTruthy();
      expect(appSettings.slugErrorMessage.getText()).toBe('Contains invalid characters.');
      component.modals.closeButton.click();
    });
  });
})();
