(function () {
  'use strict';

  var Checkbox = require('./../../../checkbox.page');

  var ForumPage = function () {
    var api = this;
    var createForumApp = $('.create-app-form');
    var forumApp = '.forum-app ';
    var iconBackground = '.thread-icon .thread-icon-background.zmdi.zmdi-circle';
    var iconForeground = '.thread-icon .thread-icon-foreground.zmdi';

    api.setting = {
      appName: createForumApp.$('#app-name-NONE'),
      active: new Checkbox(createForumApp.element(by.model('$ctrl.app.active'))),
    };

    api.threadList = {
      empty: $('span[translate="APP.FORUM.EMPTY"]'),
      list: $(forumApp + '.table .forum-threads'),
      create: $('.forum-thread-create'),
      createOnEmpty: $('a[translate="APP.FORUM.EMPTY_LINK"]'),
      threadLinks: $(forumApp + '.forum-threads').all(by.css('.thread a[ui-sref=".view({id:thread.id})"]')),
      threads: $(forumApp + '.forum-threads').all(by.css('.thread')),
      threadContextMenu: '.thread-context-menu.context-menu.dropdown.dropdown-toggle',
      contextMenu: {
        hover: by.css('.thread-status-options-list'),
        toggle: by.css('.thread-context-menu.context-menu.dropdown.dropdown-toggle'),
        pin: by.css('a[ng-click="$ctrl.pin(thread)"]'),
        unpin: by.css('a[ng-click="$ctrl.unpin(thread)"]'),
        close: by.css('a[ng-click="$ctrl.close(thread)"]'),
        reopen: by.css('a[ng-click="$ctrl.reopen(thread)"]'),
        remove: by.css('a[ng-click="$ctrl.delete(thread)"]'),
      },
      entry: {
        icon: {
          open: {
            background: by.css(iconBackground),
            foreground: by.css(iconForeground + '.zmdi-comments')
          },
          pinned: {
            background: by.css(iconBackground + '.pinned'),
            foreground: by.css(iconForeground + '.zmdi-star')
          },
          closed: {
            background: by.css(iconBackground + '.closed'),
            foreground: by.css(iconForeground + '.zmdi-lock-outline')
          }
        },
        title: by.css(forumApp + '.thread .thread-title'),
        answers: {
          icon: by.css(forumApp + '.thread .thread-comment-info .zmdi zmdi-comments'),
          count: by.css(forumApp + '.thread .thread-comment-info .comment-count')
        },
        created: {
          author: by.css(forumApp + '.thread .thread-created .thread-author')
        },
        lastAnswer: by.css(forumApp + '.thread .thread-lastanswer')
      }
    };

    api.thread = {
      create: {
        header: $(forumApp + '.forum-thread-header'),
        titleInput: $(forumApp + '#title'),
        editorInput: $(forumApp + '.forum-thread-text .note-editable'),
        attachment: {
          button: $(forumApp + '.forum-thread-attachment-trigger'),
          input: $('input[type=file]')
        },
        saveButton: $(forumApp + '.thread-save')
      },
      view: {
        title: $(forumApp + '.panel-title.thread-title'),
        status: {
          container: $(forumApp + '.thread-status .label'),
          open: $('span[translate="APP.FORUM.THREAD.OPEN"]'),
          pinned: $('span[translate="APP.FORUM.THREAD.PINNED"]'),
          closed: $('span[translate="APP.FORUM.THREAD.CLOSED"]')
        },
        text: $(forumApp + '.thread-content .rte-html-container'),
        attachments: $(forumApp + '.thread-attachments'),
        closedMessage: $(forumApp + '.thread-closed-status'),
        toOverview: $(forumApp + '.forum-thread-to-overview'),
        addAnswer: {
          title: $(forumApp + '.forum-answers-add-title'),
          avatar: $(forumApp + '.thread-add-answer-user-avatar'),
          editor: $(forumApp + '.thread-add-answer-text .note-editable'),
          saveButton: $(forumApp + '.btn-primary.thread-save')
        },
        answers: $(forumApp + '.thread-answers').all(by.css('.thread-answer')),
        answer: {
          info: $(forumApp + '.thread-answer-info'),
          author: $(forumApp + '.thread-answer-info .author'),
          creator: $(forumApp + '.thread-answer-info span[translate="APP.FORUM.THREAD.CREATOR"'),
          deleted: $(forumApp + '.thread-answer-info span[translate="APP.FORUM.THREAD.ANSWER.DELETED.LABEL"'),
          time: $(forumApp + '.thread-answer-info .thread-time'),
          text: $(forumApp + '.thread-answer-text'),
          contextMenu: {
            hover: $(forumApp + '.panel-body.thread-answer'),
            toggle: $(forumApp + '.thread-answer-context-menu.context-menu.dropdown.dropdown-toggle'),
            remove: $(forumApp + 'a[ng-click="$ctrl.deleteAnswer(answer)"]'),
          }

        },
        contextMenu: {
          hover: $(forumApp + '.panel-heading.panel-heading-main.thread-header'),
          toggle: $(forumApp + '.thread-context-menu.context-menu.dropdown.dropdown-toggle'),
          pin: $(forumApp + 'a[ng-click="$ctrl.pin()"]'),
          unpin: $(forumApp + 'a[ng-click="$ctrl.unpin()"]'),
          close: $(forumApp + 'a[ng-click="$ctrl.close()"]'),
          reopen: $(forumApp + 'a[ng-click="$ctrl.reopen()"]'),
          remove: $(forumApp + 'a[ng-click="$ctrl.delete()"]'),
        }
      }
    };

  };

  module.exports = ForumPage;

})();
