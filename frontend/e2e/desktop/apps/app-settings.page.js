(function () {
  'use strict';

  function AppSettings() {
    var api = this;

    api.name = element(by.model('$ctrl.languages[key].translations.name'));
    api.slug = element(by.model('$ctrl.app.slug'));

    api.slugErrorMessage = $('p[ng-message="pattern"]');
  }

  module.exports = AppSettings;

})();
