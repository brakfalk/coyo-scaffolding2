(function () {
  'use strict';

  var extend = require('util')._extend;

  function UpcomingEventsWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      numberOfDisplayedEvents: element(by.model('model.settings.numberOfDisplayedEvents')),
      displayOngoingEvents: element(by.model('model.settings.displayOngoingEvents')),
      fetchUpcomingEvents: element(by.model('model.settings.fetchUpcomingEvents'))
    };
  }
  module.exports = UpcomingEventsWidget;
})();
