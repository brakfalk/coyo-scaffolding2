(function () {
  'use strict';

  var login = require('../../../login.page.js');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var CalloutWidget = require('./callout-widget.page');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('callout widget', function () {
    var widgetSlot, widgetChooser, widget, calloutWidget, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      calloutWidget = new CalloutWidget(widget);

      widgetSlot.addButton.click();
      widgetChooser.selectByName('Callout');
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it ('should be created', function () {
      expect(calloutWidget.calloutHasClass('alert-success')).toBe(true);
      calloutWidget.hover();
      calloutWidget.inlineOptions.styleToggle.click();
      calloutWidget.inlineOptions.styleOption('Danger').click();

      calloutWidget.callout.click();
      calloutWidget.textarea.sendKeys('**Problem solved!** Thanks for your help.');

      navigation.viewEditOptions.saveButton.click();
      expect(calloutWidget.callout.getText()).toBe('Problem solved! Thanks for your help.');
      expect(calloutWidget.calloutHasClass('alert-danger')).toBe(true);

      // remove widget
      navigation.editView();
      widget.hover();
      widget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();

      expect(widgetSlot.allWidgets.count()).toBe(0);
    });
  });
})();
