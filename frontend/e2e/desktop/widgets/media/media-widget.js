(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var MediaWidget = require('./media-widget.page.js');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('media widget', function () {
    var widgetSlot, widgetChooser, widget, mediaWidget, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      mediaWidget = new MediaWidget(widget);
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should be created', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Media');

      // add information
      mediaWidget.informationTab.click();
      mediaWidget.changeTitle('A splendid title');
      mediaWidget.changeDescription('A splendid description');
      mediaWidget.changeLocation('A splendid location');

      // add media
      mediaWidget.mediaTab.click();
      mediaWidget.selectExampleFile();

      // save
      widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);

      expect(mediaWidget.renderedWidget.albumTitle.getText()).toBe('A splendid title');
      expect(mediaWidget.renderedWidget.albumDescription.getText()).toBe('A splendid description');
      expect(mediaWidget.renderedWidget.albumLocation.getText()).toBe('A splendid location');
      expect(mediaWidget.renderedWidget.media.isPresent()).toBeTruthy();

      // light box
      browser.executeScript('arguments[0].scrollIntoView();', mediaWidget.renderedWidget.media.getWebElement());
      mediaWidget.renderedWidget.media.click();
      mediaWidget.closeLightBoxButton.click();

      // edit
      widget.hover();
      widget.editButton.click().then(function () {
        components.modals.confirm.confirmButton.click();
      });

      // remove widget
      widget.hover();
      widget.removeButton.click().then(function () {
        components.modals.confirm.confirmButton.click();
        navigation.viewEditOptions.saveButton.click();
        expect(widgetSlot.allWidgets.count()).toBe(0);
        expect(widgetSlot.addButton.isPresent()).toBe(false);
      });
    });
  });

})();
