(function () {
  'use strict';

  var login = require('../../../login.page.js');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var ButtonWidget = require('./button-widget.page');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('button widget', function () {
    var widgetSlot, widgetChooser, widget, buttonWidget, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      buttonWidget = new ButtonWidget(widget);

      widgetSlot.addButton.click();
      widgetChooser.selectByName('Link Button');
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should be created', function () {

      buttonWidget.settings.text.sendKeys('Test Button');
      buttonWidget.settings.url.sendKeys('http://www.mindsmash.com');
      buttonWidget.settings.severityToggle.click();
      buttonWidget.settings.severityOption('Info').click();

      expect(buttonWidget.settings.preview.getText()).toBe('Test Button');
      expect(buttonWidget.buttonHasClass(buttonWidget.settings.preview, 'btn-info')).toBe(true);

      // save
      widgetSlot.widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);
      // browser.sleep(5000);
      expect(buttonWidget.input.getAttribute('value')).toBe('Test Button');
      expect(buttonWidget.buttonHasClass(buttonWidget.button, 'btn-info')).toBe(true);

      buttonWidget.hover();
      buttonWidget.inlineOptions.severityToggle.click();
      buttonWidget.inlineOptions.severityOption('Danger').click();

      buttonWidget.button.click();
      buttonWidget.input.clear();
      buttonWidget.input.sendKeys('CLICK ME');

      navigation.viewEditOptions.saveButton.click();
      expect(buttonWidget.button.getText()).toBe('CLICK ME');
      expect(buttonWidget.buttonHasClass(buttonWidget.button, 'btn-danger')).toBe(true);

      // remove widget
      navigation.editView();
      widget.hover();
      widget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();

      expect(widgetSlot.allWidgets.count()).toBe(0);
    });
  });
})();
