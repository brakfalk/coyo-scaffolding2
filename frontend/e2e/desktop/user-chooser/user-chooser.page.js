(function () {
  'use strict';

  function UserChooser(ngModel) {
    var api = this;

    api.openButton = element(by.model(ngModel));

    api.modal = {
      tabs: {
        users: {
          heading: $('uib-tab-heading[translate="USER_CHOOSER.TABS.USERS"]'),
          filterField: element(by.model('$ctrl.searchTerm')),
          filterResult: $('[ng-click="vm.toggleSelectionUser(user)"]')
        }
      },
      selectButton: $('.modal-content-wrapper [ng-click="vm.save()"]')
    };
  }

  module.exports = UserChooser;

})();
