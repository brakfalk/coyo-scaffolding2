(function () {
  'use strict';

  var login = require('../../login.page.js');
  var Navigation = require('./../navigation.page.js');
  var Profile = require('./../profile/profile.page.js');
  var testhelper = require('../../testhelper.js');

  describe('notification activity', function () {
    var navigation, profile;
    var username, password, firstname, lastname;

    beforeAll(function () {
      login.loginDefaultUser();
      var key = Math.floor(Math.random() * 1000000);
      username = 'max1.mustermann1.' + key + '@mindsmash.com';
      password = 'Secret123';
      firstname = 'Max1';
      lastname = 'Mustermann1' + key;
      testhelper.createUser(firstname, lastname, username, password, ['User']);
      navigation = new Navigation();
      profile = new Profile();
    });

    afterAll(function () {
      testhelper.deleteUsers();
    });

    it('create notification for activity and read it', function () {
      // follow Max1 Mustermann1
      browser.get('/profile/' + firstname + '-' + lastname + '/activity');
      testhelper.cancelTour();
      profile.senderActions.follow.click();
      expect(profile.senderActions.followIsSuccess).toBeTruthy();

      login.logout();

      // login as Max1 Mustermann1
      login.login(username, password);
      testhelper.cancelTour();

      // check if a notification is pending
      expect(navigation.notification.isUnseen).toBeTruthy();

      navigation.notification.open();
      navigation.notification.activity.click();

      // check if notification is new
      navigation.notification.time(0).getText().then(function (text) {
        var bool = false;
        if (text === 'a few seconds ago' || text === 'a minute ago') {
          bool = true;
        }
        expect(bool).toBe(true);
      });

      // view notification
      navigation.notification.notifications.get(0).click();
      testhelper.cancelTour();
      expect(profile.title.getText()).toBe('Ian Bold');

      login.logout();
      login.loginDefaultUser();
    });

  });

})();
