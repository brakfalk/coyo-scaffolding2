(function () {
  'use strict';
  var webdriver = require('selenium-webdriver');
  var NavigationPage = require('../navigation.page.js');
  var Colleagues = require('./colleagues.page.js');
  var Profile = require('../profile/profile.page.js');
  var testhelper = require('../../testhelper.js');
  var login = require('../../login.page.js');

  describe('colleague list', function () {
    var navigation, colleagues, profile;
    var PAGE_SIZE = 20;

    beforeAll(function () {
      navigation = new NavigationPage();
      colleagues = new Colleagues();
      profile = new Profile();

      login.loginDefaultUser();
    });

    beforeEach(function () {
      navigation.getHome();
    });

    afterEach(function () {
      testhelper.deleteUsers();
    });

    it('browse colleagues and test paging', function () {
      var promises = [];
      for (var i = 0; i < PAGE_SIZE; i++) {
        var key = Math.floor(Math.random() * 1000000);
        var username = 'max.mustermann.' + key + '@mindsmash.com';
        var password = 'Secret123';
        promises.push(testhelper.createUser('Max', 'Mustermann' + key, username, password, ['User']));
      }

      webdriver.promise.all(promises).then(function () {
        navigation.colleagues.click();
        testhelper.cancelTour();
        colleagues.filter.all.click();
        var nextPageBtn = colleagues.list.nextPage;
        // if this fails user might have not been created via service
        expect(nextPageBtn.isPresent()).toBeTruthy();
        nextPageBtn.click();

        colleagues.list.activePage().then(function (elem) {
          expect(elem.getText()).toBe('2');
        });
      });
    });

    it('browse colleague', function () {
      var key = Math.floor(Math.random() * 1000000);
      var username = 'max.mustermann.' + key + '@mindsmash.com';
      var password = 'Secret123';
      testhelper.createUser('Max', 'Mustermann' + key, username, password, ['User']);
      navigation.colleagues.click();
      testhelper.cancelTour();
      colleagues.filter.all.click();
      colleagues.list.userByName('Max Mustermann' + key).click();
      expect(profile.title.getText()).toBe('Max Mustermann' + key);
    });

  });
})();
