(function () {
  'use strict';

  var webdriver = require('selenium-webdriver');
  var Navigation = require('../navigation.page.js');
  var PageList = require('./page-list.page.js');
  var PageDetail = require('./page-details.page.js');
  var login = require('../../login.page.js');
  var testhelper = require('../../testhelper.js');

  describe('pages list', function () {
    var pageList, pageDetail, navigation;
    var PAGE_SIZE = 15;

    beforeAll(function () {
      pageList = new PageList();
      pageDetail = new PageDetail();
      navigation = new Navigation();

      login.loginDefaultUser();
      testhelper.cancelTour();
    });

    beforeEach(function () {
      navigation.getHome();
    });

    afterEach(function () {
      testhelper.deletePages();
    });


    it('pagination pagelist', function () {
      var promises = [];
      var key = Math.floor(Math.random() * 1000000);
      for (var i = 0; i < PAGE_SIZE + 1; i++) {
        promises.push(testhelper.createPage('PaginationTestPage_' + i + '_' + key));
      }

      webdriver.promise.all(promises).then(function () {
        navigation.pages.click();
        testhelper.cancelTour();
        pageList.filterAll.click();
        pageList.list.nextPage.click();
        expect(pageList.list.activePage().getText()).toBe('2');
      });
    });

    it('search page with category', function () {
      var categoryName = 'Company';
      var key = Math.floor(Math.random() * 1000000);
      var pageName = 'pagename_' + key;
      var pageDescription = 'pagedescription_' + key;
      testhelper.getCategoryId(categoryName, 'PageCategoryModel').then(function (categoryId) {
        testhelper.createPageWithSingleCategory(pageName, pageDescription, categoryId, categoryName);
        navigation.pages.click();
        testhelper.cancelTour();
        pageList.filterCategory(categoryName);

        expect(pageList.list.pageList.count()).toBe(2);

        var element;
        pageList.list.pageList.each(function (elem) {
          var el = pageList.list.pageName(elem);
          el.getText().then(function (text) {
            if (text === pageName) {
              element = el;
            }
          });
        }).then(function () {
          element.click();
        });
      });

      expect(pageDetail.title.isPresent()).toBe(true);
      expect(pageDetail.title.getText()).toBe(pageName);
      expect(pageDetail.description.getText()).toBe(pageDescription);
    });

    it('search page with multiple categories', function () {
      var categoryCompanyName = 'Company';
      var categoryHelpName = 'Help';
      var key = Math.floor(Math.random() * 1000000);
      var pageName = 'pagename_' + key;
      var pageDescription = 'pagedescription_' + key;
      var categories = [];
      var categoryModel = 'PageCategoryModel';
      testhelper.getCategoryId(categoryCompanyName, categoryModel).then(function (categoryId) {
        categories.push({categoryName: categoryCompanyName, categoryId: categoryId});
        testhelper.getCategoryId(categoryHelpName, categoryModel).then(function (categoryId) {
          categories.push({categoryName: categoryHelpName, categoryId: categoryId});
          testhelper.createPageWithMultipleCategories(pageName, pageDescription, categories);
          navigation.pages.click();
          testhelper.cancelTour();
          pageList.filterCategory(categories[0].categoryName);
          pageList.filterCategory(categories[1].categoryName);
          expect(pageList.list.pageList.count()).toBeGreaterThan(1);
        });
      });
    });

  });

})();
