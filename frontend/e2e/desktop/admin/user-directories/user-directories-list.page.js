(function () {
  'use strict';

  var repeater = 'row in $ctrl.userDirectories track by row.id';

  module.exports = {
    get: function () {
      browser.get('/admin/user-directories');
      require('../../../testhelper.js').disableAnimations();
    },
    nameFilter: element(by.model('$ctrl.searchTerm')),
    directoryTotal: $('counter span.hidden-xs'),
    createButton: $('.fb-actions-inline a[ui-sref="admin.user-directories.create"]'),
    table: {
      rows: {
        get: function (index) {
          var row = $$('tr[ng-repeat="' + repeater + '"]').get(index);
          return {
            row: row,
            name: row.$$('td').get(0),
            type: row.$$('td').get(1),
            status: row.$$('td').get(3),
            options: function () {
              var el = row.$$('td').get(4);
              return {
                open: function () {
                  browser.actions().mouseMove(el).perform();
                  el.$('.dropdown-toggle').click();
                },
                editOption: el.$('span[translate="ADMIN.USER_DIRECTORIES.OPTIONS.EDIT.MENU"]'),
                deleteOption: el.$('span[translate="DELETE"]'),
                activateOption: el.$('span[translate="ADMIN.USER_DIRECTORIES.OPTIONS.ACTIVATE.MENU"]'),
                deactivateOption: el.$('span[translate="ADMIN.USER_DIRECTORIES.OPTIONS.DEACTIVATE.MENU"]')
              };
            }
          };
        },
        names: function () {
          return $$('tr[ng-repeat="' + repeater + '"]').map(function (row) {
            return row.$$('td').get(0).getText();
          });
        },
        count: function () {
          return element.all(by.repeater('' + repeater + '')).count();
        }
      }}
  };

})();
