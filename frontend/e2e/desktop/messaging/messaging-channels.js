(function () {
  'use strict';

  var login = require('./../../login.page.js');
  var sidebar = require('./messaging-sidebar.page.js');
  var testhelper = require('../../testhelper');
  var NavigationPage = require('../navigation.page.js');

  describe('messaging channels', function () {
    var colleague1, colleague2, colleague3, username1, password1, navigation;

    beforeAll(function () {
      navigation = new NavigationPage();
      login.loginDefaultUser();

      // create a few users
      var key1 = Math.floor(Math.random() * 1000000);
      username1 = 'max1.mustermann1.' + key1 + '@mindsmash.com';
      password1 = 'Secret123';
      colleague1 = 'Max1 Mustermann1' + key1;
      testhelper.createUser('Max1', 'Mustermann1' + key1, username1, password1, ['User']);

      var username2 = 'max2.mustermann2.' + key1 + '@mindsmash.com';
      var password2 = 'Secret123';
      colleague2 = 'Max2 Mustermann2' + key1;
      testhelper.createUser('Max2', 'Mustermann2' + key1, username2, password2, ['User']);

      var username3 = 'max3.mustermann3.' + key1 + '@mindsmash.com';
      var password3 = 'Secret123';
      colleague3 = 'Max3 Mustermann3' + key1;
      testhelper.createUser('Max3', 'Mustermann3' + key1, username3, password3, ['User']);

      // move mouse over message sidebar to trigger scroll bar disappearance where it does not hurt
      browser.actions().mouseMove(sidebar.channelsView.createChannel, {x: 0, y: 0}).perform();
    });

    beforeEach(function () {
      navigation.getSearch();
    });

    afterAll(function () {
      testhelper.deleteUsers();
    });

    it('should see channels view', function () {
      expect(sidebar.channelsView.parent.isPresent()).toBe(true);
    });

    it('should start single channel', function () {
      // Open form
      sidebar.channelsView.createChannel.click();
      expect(sidebar.createChannelView.parent.isPresent()).toBe(true);
      expect(sidebar.createChannelView.subject.isPresent()).toBe(false);

      // Select user from list
      sidebar.createChannelView.userItem.get(0).click();
      expect(sidebar.channelView.parent.isPresent()).toBe(true);
    });

    it('should start group channel', function () {
      // Open form
      sidebar.channelsView.createChannel.click();
      // workaround: move the mouse so that the help tooltip does not stay open
      // (the help icon is at the same place the create icon was before the last click)
      browser.actions().mouseMove($('body'), {x: 0, y: 0}).perform();
      sidebar.createChannelView.createGroupChannel.click();
      expect(sidebar.createChannelView.parent.isPresent()).toBe(true);
      expect(sidebar.createChannelView.subject.isPresent()).toBe(true);

      var subject = 'Test ' + Math.floor(Math.random() * 1000000);

      // Set subject
      sidebar.createChannelView.subject.sendKeys(subject);

      // Select user from list
      sidebar.createChannelView.userItem.get(0).click();

      // Check selected element and submit button
      expect(sidebar.createChannelView.userItem.count()).toBe(1);

      browser.sleep(100); // wait a little bit

      expect(sidebar.createChannelView.submit.isPresent()).toBe(true);
      sidebar.createChannelView.submit.click();

      expect(sidebar.channelView.parent.isPresent()).toBe(true);
    });

    it('should start group channel and write message', function () {
      var subject = 'Informations ' + Math.floor(Math.random() * 1000000);
      var message = 'Hello everybody ' + Math.floor(Math.random() * 1000000);
      login.logout();
      login.login(username1, password1);

      // Open form
      sidebar.channelsView.createChannel.click();
      // create group
      sidebar.createChannelView.createGroupChannel.click();
      // enter subject
      sidebar.createChannelView.subject.sendKeys(subject);
      // choose user
      sidebar.createChannelView.findUser.sendKeys(colleague2);
      sidebar.createChannelView.searchedUser.get(0).click();
      sidebar.createChannelView.findUser.sendKeys(colleague3);
      sidebar.createChannelView.searchedUser.get(0).click();
      sidebar.createChannelView.submit.click();

      expect(sidebar.channelView.contentContains(colleague1).isPresent()).toBe(true);
      expect(sidebar.channelView.contentContains(colleague2).isPresent()).toBe(true);
      expect(sidebar.channelView.contentContains(colleague3).isPresent()).toBe(true);

      // enter and send message
      sidebar.channelView.newMessage.sendKeys(message);
      sidebar.channelView.submit.click();

      expect(sidebar.channelView.message(message).isPresent()).toBe(true);

      login.logout();
      login.loginDefaultAdminUser();
    });

    it('should send and receive message', function () {
      // Open channel
      sidebar.channelsView.channels.get(0).click();
      expect(sidebar.channelView.parent.isPresent()).toBe(true);

      var message = 'Hello World ' + Math.floor(Math.random() * 1000000);
      sidebar.channelView.newMessage.sendKeys(message, protractor.Key.ENTER);

      browser.sleep(100); // wait a little bit
      expect(element(by.cssContainingText('.message', message)).isPresent()).toBe(true);
    });
  });

})();
