(function () {
  'use strict';

  var login = require('../../login.page.js');
  var testhelper = require('../../testhelper');
  var components = require('../../components.page');
  var MobileNavigation = require('../mobile-navigation.page');
  var Profile = require('../../desktop/profile/profile.page');
  var PersonalTimeline = require('../../desktop/timeline/timeline-personal.page');
  var TimelineItem = require('../../desktop/timeline/timeline-item.page');

  describe('mobile profile', function () {
    var mobileNavigation, profile, personalTimeline;
    var firstname, lastname, email, password;

    beforeEach(function () {
      mobileNavigation = new MobileNavigation();
      personalTimeline = new PersonalTimeline();
      profile = new Profile();
      login.loginDefaultUser();

      var key = Math.floor(Math.random() * 1000000);
      firstname = 'Maxi';
      lastname = 'Mustermann' + key;
      email = 'maxi.mustermann.mobil' + key + '@mindsmash.com';
      password = 'Secret123';
      testhelper.createUser(firstname, lastname, email, password, ['User']);
      mobileNavigation.logout();
      login.login(email, password);
    });

    afterEach(function () {
      mobileNavigation.logout();
      login.loginDefaultUser();
      testhelper.deleteUsers();
    });

    it('navigate to profile', function () {
      mobileNavigation.naviagtion.open();
      mobileNavigation.naviagtion.groupMain.open().then(function () {
        mobileNavigation.naviagtion.groupMain.profile.click();
        testhelper.cancelTour();
        var username = firstname + ' ' + lastname;
        expect(profile.profileName.getText()).toBe(username);
      });

    });

    it('navigate to infotab of profile', function () {
      mobileNavigation.naviagtion.open();
      mobileNavigation.naviagtion.groupMain.open().then(function () {
        mobileNavigation.naviagtion.groupMain.profile.click();
        testhelper.cancelTour();

        profile.info.click();
        expect(profile.contact.form.isPresent()).toBeTruthy();
        expect(profile.information.form.isPresent()).toBeTruthy();
        expect(profile.work.form.isPresent()).toBeTruthy();
      });
    });

    it('create post on own profile wall', function () {
      mobileNavigation.naviagtion.open();
      mobileNavigation.naviagtion.groupMain.open().then(function () {
        mobileNavigation.naviagtion.groupMain.profile.click();
        testhelper.cancelTour();

        profile.activity.click();
        expect(profile.mobileCreatePostButton.isPresent()).toBeTruthy();

        var message = 'Hello world and hello everybody';
        profile.mobileCreatePostButton.click();
        profile.wall.postOnWallTextarea.first().sendKeys(message);
        components.modals.confirm.confirmButton.click();

        var timelineItem = TimelineItem.create(personalTimeline.stream, 0);
        expect(timelineItem.message.getText()).toBe(message);
      });
    });

  });

})();
