(function () {
  'use strict';

  function Checkbox(element) {
    var api = this;
    api.click = function () {
      element.$('.coyo-checkbox').click();
    };
    api.isChecked = function () {
      return element.$('.coyo-checkbox').getAttribute('class').then(function (clazz) {
        return /checked/.test(clazz);
      });
    };
  }

  module.exports = Checkbox;

})();
