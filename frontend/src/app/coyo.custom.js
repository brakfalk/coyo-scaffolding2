(function (angular) {
  'use strict';

  angular.module('coyo.custom', ['coyo.app'])
    .run(function ($log) {
      $log.debug('Starting custom coyo application...');
    });

})(angular);
